import React from "react";

const App = () => {
  const MALE_LIFE_EXPECTANCY = 80;
  const FEMALE_LIFE_EXPECTANCY = 85;

  const currentYear = new Date().getFullYear();

  const [sGender, setsGender] = React.useState<"MALE" | "FEMALE">("MALE");
  const [sDate, setsDate] = React.useState("");

  const [showSettings, setShowSettings] = React.useState(false);
  const [gender, setGender] = React.useState<"MALE" | "FEMALE">("MALE");
  const [dateOfBirth, setDateOfBirth] = React.useState(new Date());

  return (
    <div className="flex flex-col w-screen h-screen p-8 overflow-y-auto bg-red-50">
      <button className="self-end text-4xl text-red-300 transition-all hover:text-red-200">
        <i
          className="uil uil-setting"
          onClick={() => {
            setShowSettings(true);
          }}
        ></i>
      </button>
      <div className="flex flex-col items-center justify-center flex-grow">
        <h1 className="mb-16 text-6xl font-normal text-red-700">
          Life <br className="sm:hidden" /> Progress{" "}
        </h1>
        <div className="grid grid-cols-10 gap-2">
          {Array.from(
            new Array(
              gender === "MALE" ? MALE_LIFE_EXPECTANCY : FEMALE_LIFE_EXPECTANCY
            )
          ).map((_, index) => (
            <div
              key={index}
              className={`col-span-1 h-4 w-4 rounded-full sm:h-6 sm:w-6 sm:rounded-lg bg-gradient-to-br  ${
                index <
                currentYear - (dateOfBirth?.getFullYear() ?? currentYear)
                  ? "from-red-100 to-red-50 shadow-inner"
                  : "from-red-300 to-red-500 shadow-md"
              }`}
            />
          ))}
        </div>
      </div>

      <form
        className={`absolute h-screen w-screen top-0 left-0 backdrop-filter backdrop-blur-md backdrop-brightness-90 transition-all ${
          showSettings ? "visible opacity-100" : "invisible opacity-0"
        }`}
        onSubmit={(e) => {
          setGender(sGender);
          setDateOfBirth(new Date(sDate));
          setShowSettings(false);
          e.preventDefault();
        }}
      >
        <div className="absolute transform -translate-x-1/2 -translate-y-1/2 bg-white top-1/2 left-1/2 rounded-3xl bg-opacity-80">
          <div className="p-8 text-center">
            <div className="mb-8">
              <div className="mb-4 text-xl text-black text-opacity-80">
                Gender
              </div>

              <div className="flex justify-around">
                <button
                  type="button"
                  style={{
                    WebkitTapHighlightColor: "transparent",
                  }}
                  className={`flex flex-col transition-colors rounded-2xl p-4 items-center ${
                    sGender === "MALE"
                      ? "text-blue-300 shadow-inner bg-gradient-to-tl from-blue-50 to-blue-100"
                      : "text-gray-300"
                  }`}
                  onClick={() => {
                    setsGender("MALE");
                  }}
                >
                  <i
                    className={`uil uil-mars text-5xl transition-colors ${
                      sGender === "MALE" ? "text-blue-400" : "text-gray-300"
                    }`}
                  ></i>
                  Male
                </button>

                <button
                  type="button"
                  style={{
                    WebkitTapHighlightColor: "transparent",
                  }}
                  className={`flex flex-col transition-colors rounded-2xl p-4 items-center ${
                    sGender === "FEMALE"
                      ? "text-pink-300 shadow-inner bg-gradient-to-tl from-pink-50 to-pink-100"
                      : "text-gray-300"
                  }`}
                  onClick={() => {
                    setsGender("FEMALE");
                  }}
                >
                  <i
                    className={`uil uil-venus text-5xl transition-colors ${
                      sGender === "FEMALE" ? "text-pink-400" : "text-gray-300"
                    }`}
                  ></i>
                  Female
                </button>
              </div>
            </div>
            <div className="mb-6">
              <div className="mb-4 text-xl text-black text-opacity-80">
                Date of Birth
              </div>
              <input
                type="date"
                className="p-2 text-red-400 shadow-inner outline-none bg-gradient-to-br from-red-200 to-red-100 rounded-2xl"
                value={sDate}
                onChange={(e) => {
                  setsDate(e.target.value);
                }}
              />
            </div>
          </div>
          <button
            type="submit"
            className="absolute bottom-0 p-2 px-8 text-white transition-all transform -translate-x-1/2 shadow-md bg-gradient-to-br from-red-300 to-red-500 hover:from-red-300 hover:to-red-400 rounded-2xl left-1/2 translate-y-1/3"
          >
            Save
          </button>
        </div>
      </form>
    </div>
  );
};

export default App;
