# life-progress

Inspired by life calendars. Set your date of birth, and your gender, and it will display a grid.
This is grid represents your life expectancy (hence picking your gender) according to INSEE, and each square represents one year.

Seeing your elapsed time and your estimated time left should motivate you to make the most out of it.

No technical highlights.

Deployed at [https://life-progress.vercel.app/](https://life-progress.vercel.app/)

![alt text](image.png)
